# TeXová šablona pro tisk rozvrhu z ISu

Tyto soubory použijete, pokud si chcete z ISu stáhnout rozvrh
ve formě zdrojového kódu pro TeX a sami si jej upravit a přeložit.

Pokud nevíte, co je TeX, není tento balík pro vás :-)

Tento repozitář je poskytovaný tak jak je v dobré víře,
že bude někomu k užitku. Je však *nepodporovaný* vývojovým týmem ISu.
Nemá cenu zde hlásit jakékoli chyby a myslet si, že se jimi někdo
bude zabývat. Nicméně návrhy na úpravy ve formě merge requestů se možná
dočkají nějaké pozornosti, bude-li čas :-)

Autorem původních šablon je Zuzana Popelková a Petr Sojka.

