RUN_COUNT=2
DO_PDF_OPT=1
NAME=is_tisk

FIGS=
PNGFIGS=
DOCUMENT_DEPENDENCIES=${NAME}.tex istimetables.tex istimetables.conf Makefile ${FIGS} ${PNGFIGS}
DOCUMENT_METADATA=${NAME}.{aux,bbl,blg,log,out}
SHELL=/bin/bash
#PDF_PRODUCER=pdflatex
PDF_PRODUCER=xelatex
PDF_PRODUCER_ARGUMENTS=
PDF_OPTIMIZER=pdfopt
PDF_OPTIMIZER_ARGUMENTS=


.PHONY: all pdf clean vlna

all: pdf
pdf: ${NAME}.pdf

clean:
	@echo -e "\n### Zahajuji uklid... ###"
	-rm ${DOCUMENT_METADATA}
	@echo -e "### Uklid hotov. ###"

vlna:
	@echo -e "\n### Spoustim vlnu na zdrojove kody... ###"
	vlna -r -l -v KkSsVvZzOoUuAaIi ${NAME}.tex
	@echo -e "### Uprava zdrojovych kodu vlnou hotova. ###"


${NAME}.pdf: ${DOCUMENT_DEPENDENCIES}
	@echo -e "\n### Vytvarim PDF vystup... ###"
	@echo -e "\n### Odstranuji stara metadata... ###"
	-rm ${DOCUMENT_METADATA}
	@echo -e "### Odstraneni starych metadat hotovo. ###"
	@echo -e "\n### Prekladam... ###"
	${PDF_PRODUCER} ${PDF_PRODUCER_ARGUMENTS} ${NAME}.tex
#	bibtex ${NAME}
ifneq (${RUN_COUNT}, 1)
	for i in `seq $$[${RUN_COUNT}-1]`; do \
		echo; \
		${PDF_PRODUCER} ${PDF_PRODUCER_ARGUMENTS} ${NAME}.tex; \
	done
endif
	@echo -e "### Preklad hotov. ###"
ifeq (${DO_PDF_OPT}, 1)
	@echo -e "\n### Optimalizuji PDF vystup... ###"
	mv ${NAME}.pdf ${NAME}-in.pdf
	${PDF_OPTIMIZER} ${PDF_OPTIMIZER_ARGUMENTS} ${NAME}-in.pdf ${NAME}.pdf
	rm ${NAME}-in.pdf
	@echo -e "### Optimalizace PDF vystupu hotova. ###"
endif
	@echo -e "\n### PDF vystup hotov. ###"

svn:	${DOCUMENT_DEPENDENCIES}	
	svn ci

archive:	${NAME}.pdf
	rm -rf ${NAME}-archive.zip	
	zip -9 ${NAME}-version${VERSION} ${DOCUMENT_DEPENDENCIES} 
	@echo -e "\n### ${NAME}-version${VERSION} hotov pro easychair upload. ###"


